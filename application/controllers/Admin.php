<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_Users');
        $this->load->library('form_validation');

        // mencegah user masuk tidak login
        if($this->session->has_userdata('isLoggin') != true){
            redirect('auth');
        }
    }

    public function index()
    {

        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'admin/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'admin/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'admin/index.html';
            $config['first_url'] = base_url() . 'admin/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->M_Users->total_rows($q);
        $admin = $this->M_Users->get_limit_data($config['per_page'], $start, $q);

        // ACTIVE  MENU
        $menu = [
            'user' => 'active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'admin_data' => $admin,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        // var_dump($data);die;
        
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/admin', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('admin/users_list', $data);
        $this->load->view('tamplates/footer');
    }

    public function read($id) 
    {
        $row = $this->M_Users->get_by_id($id);
        if ($row) {
            $data = array(
		'id_users' => $row->id_users,
		'namalengkap' => $row->namalengkap,
		'username' => $row->username,
		'password' => $row->password,
		'email' => $row->email,
		'akses' => $row->akses,
		'isActive' => $row->isActive,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('admin/users_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin'));
        }
    }

    public function create() 
    {
        // var_dump($_POST);die;

        // WAJIB ADA DI SETIAP METHOD
        $menu = [
            'user' => 'active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // WAJIB ADA

        $data = array(
            'button' => 'Create',
            'action' => site_url('admin/create_action'),
            'id_users' => set_value('id_users'),
            'namalengkap' => set_value('namalengkap'),
            'username' => set_value('username'),
            'password' => set_value('password'),
            'email' => set_value('email'),
            'akses' => set_value('akses'),
	    // 'created_at' => set_value('created_at'),
	    // 'updated_at' => set_value('updated_at'),

        // WAJIB ADA DI SETIAP METHOD
        'menu_view' => $menu, // MENU DI OPER KE SINI
        'users' => $user
	);
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/admin', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('admin/users_form', $data);
        // $this->load->view('tamplates/footer');
    }
    
    public function create_action() 
    {
        // var_dump($_POST);die;
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
            'namalengkap' => $this->input->post('namalengkap',TRUE),
            'username' => $this->input->post('username',TRUE),
            'password' => $this->input->post('password',TRUE),
            'email' => $this->input->post('email',TRUE),
            'akses' => $this->input->post('akses',TRUE),
            'isActive' => $this->input->post('isActive',TRUE),
		// 'created_at' => $this->input->post('created_at',TRUE),
		// 'updated_at' => $this->input->post('updated_at',TRUE),
	    );
            $options = [
                'cost' => 5,
            ];
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT, $options);
            // var_dump($data);die;
            $this->M_Users->insert($data);
            $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil ditambahkan!', 'User berhasil ditambahkan!', 'success');
                </script>
            ");
            redirect(site_url('admin'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_Users->get_by_id($id);

        if ($row) {
            $data = array(
            'button' => 'Update',
            'action' => site_url('admin/update_action'),
            'id_users' => set_value('id_users', $row->id_users),
            'namalengkap' => set_value('namalengkap', $row->namalengkap),
            'username' => set_value('username', $row->username),
            'password' => set_value('password', $row->password),
            'email' => set_value('email', $row->email),
            'akses' => set_value('akses', $row->akses),
            'isActive' => set_value('isActive', $row->isActive)
		// 'created_at' => set_value('created_at', $row->created_at),
		// 'updated_at' => set_value('updated_at', $row->updated_at),
	    );
            $this->load->view('admin/users_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_users', TRUE));
        } else {
            $data = array(
		'namalengkap' => $this->input->post('namalengkap',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'email' => $this->input->post('email',TRUE),
		'akses' => $this->input->post('akses',TRUE),
		'isActive' => $this->input->post('isActive',TRUE),
		// 'created_at' => $this->input->post('created_at',TRUE),
		// 'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->M_Users->update($this->input->post('id_users', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_Users->get_by_id($id);

        if ($row) {
            $this->M_Users->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('namalengkap', 'namalengkap', 'trim|required');
    $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]', [
        'is_unique' => 'Username Sudah terpakai!'
    ]);
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]', [
        'is_unique' => 'Email Sudah digunakan!'
    ]);
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]', [
        'min_length' => 'Password terlalu pendek. Minimal 5 huruf/angka!'
    ]);
	// $this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	// $this->form_validation->set_rules('updated_at', 'updated at', 'trim|required');

	$this->form_validation->set_rules('id_users', 'id_users', 'trim');
	$this->form_validation->set_error_delimiters('<small class="text-danger pl-1">', '</small>');
    }

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2021-07-29 01:53:51 */
/* http://harviacode.com */