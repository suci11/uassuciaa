        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Users List
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Users
                    <!-- <button type="button" class="btn btn-gradient-primary btn-fw float-right">Tambah User</button> -->
                    <a href="<?= base_url('admin/create') ?>" class="btn btn-gradient-primary btn-fw float-right">Tambah User</a>
                    </h4><br>
                    <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Namalengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Akses</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($admin_data as $admin)
                        {
                            ?>
                        <tr align="center">
                            <td width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $admin->namalengkap ?></td>
                            <td><?php echo $admin->username ?></td>
                            <td><?php echo $admin->email ?></td>
                            <td><?php echo $admin->akses ?></td>
                            <td style="text-align:center" width="200px">
                                <?php 
                                echo anchor(site_url('admin/read/'.$admin->id_users),'Read', 'class="btn btn-gradient-primary btn-sm"');
                                echo ' | '; 
                                echo anchor(site_url('admin/update/'.$admin->id_users),'Update', 'class="btn btn-gradient-success btn-sm"'); 
                                echo ' | '; 
                                ?>
                                <?php if($admin->username == $this->session->userdata('username')) : ?>
                                    <button class="btn btn-gradient-secondary btn-sm">You</button>
                                <?php else : ?>
                                    <a href="<?= base_url('admin/delete/') . $admin->id_users; ?>" style="text-decoration:none" 
                                    class="btn btn-gradient-danger btn-sm hapus" onclick="return confirm('Data akan dihapus?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                      <!-- <i class="fas fa-trash"></i> -->Delete
                                    </a>
                                <?php endif; ?>
                                </td>
                              </tr>
                            <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">Keterangan Akses:</h5>
                    <table class="table table-striped">
                      <tbody>
                        <tr>
                          <td>Admin : </td>
                          <td>1</td>
                        </tr>
                        <tr>
                          <td>Operator Gudang : </td>
                          <td>2</td>
                        </tr>
                        <tr>
                          <td>Kepala Gudang : </td>
                          <td>3</td>
                        </tr>
                        <tr>
                          <td>Penjaga Gudang : </td>
                          <td>4</td>
                        </tr>
                        <tr>
                          <td>Dept. Penjualan : </td>
                          <td>5</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         