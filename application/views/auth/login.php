    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
<?= $this->session->flashdata('pesan'); ?>
                <div class="brand-logo">
                  <img src="<?= base_url('assets-app/'); ?>assets/images/logo.svg">
                </div>
                <h4>Hello! Silahkan Login!</h4>
                <form class="pt-3" action="<?= base_url('auth/prosessLogin'); ?>" method="POST">
                  <div class="form-group">
                    <input type="text" class="form-control form-control-lg" placeholder="Username" name="username">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" name="password">
                  </div>
                  <div class="mt-3">
                    <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" type="submit">LOGIN</button>
                  </div>
                  <!-- <div class="text-center mt-4 font-weight-light"> Belum Punya Akun? <a href="<?= base_url('auth/registration') ?>" class="text-primary">Daftar Disini!</a>
                  </div> -->
                </form>

              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
