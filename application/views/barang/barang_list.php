        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Operator Gudang
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">OP
                <?php echo anchor(site_url('barang/create'),'Create', 'class="btn btn-primary float-right"'); ?>
                    </h4><br>
                    <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>No</th>
                            <th>Merk</th>
                            <th>Ukuran</th>
                            <th>Jenis</th>
                            <th>Warna</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
<?php
            foreach ($barang_data as $barang)
            {
                ?>
                <tr>
      <td width="80px"><?php echo ++$start ?></td>
      <td><?php echo $barang->merk ?></td>
      <td><?php echo $barang->ukuran ?></td>
      <td><?php echo $barang->jenis ?></td>
      <td><?php echo $barang->warna ?></td>
      <td><?php echo $barang->created_at ?></td>
      <td><?php echo $barang->updated_at ?></td>
      <td style="text-align:center" width="200px">
        <?php 
        echo anchor(site_url('barang/read/'.$barang->id),'Read'); 
        echo ' | '; 
        echo anchor(site_url('barang/update/'.$barang->id),'Update'); 
        echo ' | '; 
        echo anchor(site_url('barang/delete/'.$barang->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
        ?>
      </td>
    </tr>
                <?php
            }
            ?>
                      </tbody>
                    </table>
                            <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
              </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         